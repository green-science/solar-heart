#include <SD.h> // errors sorting library
#include <Wire.h>
#include <RTC.h> // not sure which library to use here

#define rest_period 120000 // interval between data pulls in ms 120,000 is 2 mins
#define display_serial 1
#define start_command 0 

#define redLED 2
#define greenLED 3
#define v_in_pin 1
#define R1 98000.00 // measured resistance of R1 (listed as 100K ohms)
#define R2 98000.00 // measured resistant of R2 (listed as 100k ohms)

// real time clock object
const int chip_select = 10; // using pin 10 to read from the SD card
File logfile; // defining name of file 

void error(char * string)
{
  Serial.print("error: ");
  Serial.println(string);
  
  digitalWrite(redLED, HIGH); //turn on red led to signal error

  while(1); // halt, sits in it forever until restarted
}

void setup(void)
{
  Serial.begin(9600);
  Serial.println();
  #if start_command
    Serial.println("type any character to start")
    while (!Serial.available());
  #endif

  Serial.print("poking SD card...");
  pinMode(10, OUTPUT);

  if (!SD.begin(chip_select)) // see if the card is available access, looks for a FAT16/FAT32 partition
    Serial.println("card did not wake up, try harder");
}

Serial.println("I'm up and ready to log");

char filename[ ] = "loggedvoltage00.CSV";
for (uint8_t i = 0; i < 100; i++) { // defining i as 8 bit integer to set up iterations for filename
  filename[14] = i/10 + '0';
  filename[15] = i%0 + '0'; 
  if (!SD.exists(filename)) { //check to make sure that filename does not already exist
    logfile = SD.open(filenamte, FILE_WRITE);
    break; // to exit the loop
  }
}

if (!logfile) { // error for if it cannot write to file
  error("I got writers block and could not make the file")

}

Serial.print("Writing data to: ");
Serial.println("filename");

//now use the wire library to start the Real Time Clock 

Wire.begin();
if (!RTC.begin()) {
  logfile.println("Could not wake up RTC");
#if display_serial
  Serial.println("Could not wake up RTC");
#endif //display_serial
}

//make header in CSV format to be able to use data in a spreadsheet
// millis is millisesconds since arduino started, time is from RTC

  logfile.println("millis,time,voltage,current");
#if display_serial
  Serial.println("millis,time,voltage,current");
#endif display_serial //try to write header of file
  if (logfile.writeError || !logfile.sync()) {
    error("write header");
  }

//set LED pins to outputs so they can be powered on and off 
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);

// main loop

void loop(void)
{
  DateTime now;

  // delay for amount of time wanted between readings
  dealy((rest_period -1) - (millis() % rest_interval));

  digitalWrite(greenLED, HIGH); // to signify that the arduino is writing data

  //log the amount of time since the arduino has started, no necessary but migt be helpful
  uint32_t m = millis();
  logfile.print(m); //log milliseconds since start
  logfile.print(", ");
#if display_serial
  Serial.print(m);
  Serial.print(", ");
#endif

  //check the time
  now = RTC.now();
  //log time
  logfile.print(now.get());
  logfile.print(", ");
  logfile.print(now.year(), DEC);
  logfile.print("/");
  logfile.print(now.month(), DEC);
  logfile.print("/");
  logfile.print(now.day(), DEC);
  logfile.print(" ");
  logfile.print(now.hour(), DEC);
  logfile.print(":");
  logfile.print(now.minute(), DEC);
  logfile.print(":");
  logfile.print(now.second(), DEC);
#if display_serial
  Serial.print(now.get());
  Serial.print(", ");
  Serial.print(now.year(), DEC);
  Serial.print("/");
  Serial.print(now.month(), DEC);
  Serial.print("/");
  Serial.print(now.day(), DEC);
  Serial.print(" ");
  Serial.print(now.hour(), DEC);
  Serial.print(":");
  Serial.print(now.minute(), DEC);
  Serial.print(":");
  Serial.print(now.second(), DEC);
#endif //display_serial

//now log the sensor data
//code for measuring voltage from https://create.arduino.cc/projecthub/next-tech-lab/voltmeter-using-arduino-00e7d1
int voltage_value = analogRead(v_in_pin);
  v_out = (voltage_value * 5.11) / 1024.00; //formula for calculating voltage out (V+) referece is 5.11v
  float v_in = v_out / (R2/(R1+R2)); // to calculate voltage_in using voltage devider 
  //float I = v_in / (R1+R2)

  logfile.print(", "); 
  logfile.print(v_in);
  //logfile.print(", ")
  //logfile.print(I)

#if display_serial
  Serial.print(", "); 
  Serial.print(Vin);
  //Serial.print(", ")
  //Serial.print(I)
#endif 

   
  digitalWrite(greenLED, LOW)
}
